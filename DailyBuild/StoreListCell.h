//
//  StoreListCell.h
//  CityOnSale
//
//  Created by Jagsonics on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreListCell : UITableViewCell
{
    IBOutlet UILabel *storeName;
    IBOutlet UILabel *storeDiscount;
    IBOutlet UILabel *storeType;
    IBOutlet UILabel *storePriceRange;
    IBOutlet UIImageView *storeLogo;
    IBOutlet UIView *storeBgView;
}
@property (nonatomic,strong) IBOutlet UILabel *storeName;
@property (nonatomic,strong) IBOutlet UILabel *storeDiscount;
@property (nonatomic,strong) IBOutlet UILabel *storeType;
@property (nonatomic,strong) IBOutlet UILabel *storePriceRange;
@property (nonatomic,strong) IBOutlet UIImageView *storeLogo;
@property (nonatomic,strong)  IBOutlet UIView *storeBgView;

@end
