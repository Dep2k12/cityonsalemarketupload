//
//  SuperListViewController.h
//  CityOnSale
//
//  Created by Jagsonics on 23/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SuperViewController.h"
#import "StoreListCell.h"
#import "StoreDetailViewController.h"

@interface SuperListViewController : SuperViewController<UITableViewDelegate,UITableViewDataSource>

{

    IBOutlet UITableView *storesTable;
 //   IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UILabel *navTitleLabel;
    NSString *navTitle;
    
    NSString *categoryId;
    NSString *marketId;
    NSMutableArray *storesArray;
  

   
  
    
}
@property (nonatomic,strong) NSString *categoryId;
@property (nonatomic,strong) NSString *marketId; 
@property (nonatomic,strong) NSString *navTitle;
-(IBAction)backButtonClick:(id)sender;
@end

