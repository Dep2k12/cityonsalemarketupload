//
//  UIView+FindAndResignFirstResponder.h
//  CityOnSale
//
//  Created by Jagsonics on 15/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FindAndResignFirstResponder)

- (BOOL)findAndResignFirstResponder;

@end
