//
//  StoreDetailViewController.h
//  CityOnSale
//
//  Created by Jagsonics on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SuperViewController.h"

@interface StoreDetailViewController : SuperViewController
{
    IBOutlet UIView * bgCenterView;
    IBOutlet UITableView *myTableView;
   // IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UILabel *storeTypeLabel;
    IBOutlet UILabel *storePriceLabel;
    IBOutlet UILabel *storePhoneNumberLabel;
    IBOutlet UILabel *storeAddressLabel;
    IBOutlet UITextView *storeDescriptionTextView;
    IBOutlet UITextView *storeUpdatesTextView;
    IBOutlet UIImageView *storeLogoView;
    IBOutlet UILabel *noPhotoLabel;
    IBOutlet UILabel *navTitle;
    IBOutlet UIButton *codeBtn;
    IBOutlet UILabel *codeLabel;
    
    
    
    
    NSString *couponCode;
    NSString *storeId;
    NSArray *imageUrlArray;
    NSString *storeName;
    
}

@property(nonatomic,retain) NSString *storeId;
@property(nonatomic,retain) NSString *storeName;

-(IBAction)getCoupon:(id)sender;
@end
