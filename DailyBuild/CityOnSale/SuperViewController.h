//
//  SuperViewController.h
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperViewController : UIViewController
{
    NSError *networkError;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    NSString *serviceEndNotifName;
    NSString *requestUrl;
    NSString *authToken;
    NSString *userName;
    
}

-(IBAction)returnKeyPressed:(id)sender;
-(IBAction)backButtonClick:(id)sender;


-(void) callServiceWiithUrlString;
-(void) createJsonFromData:(NSData *)responseData;

@end
