//
//  SubcategoryViewController.m
//  CityOnSale
//
//  Created by APPLE Mac Mini on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SubcategoryViewController.h"
#import "SBJson.h"
@interface SubcategoryViewController ()


@end

@implementation SubcategoryViewController
@synthesize marktetId;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString *authToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    dispatch_async(kBgQueue, ^{
        
        requestUrl = [NSString stringWithFormat:@"%@?username=%@&authToken=%@",getMainCategoriesUrl,userName,authToken];
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:  requestUrl] options:0 error:&error];
        networkError = error;
        [ self performSelectorOnMainThread:@selector(fetchedMainCategoriesData:) 
                                withObject:data waitUntilDone:YES]; 
        
    });    
}

#pragma mark fetchers

-(void)fetchedMainCategoriesData:(NSData *)responseData
{
    [activityIndicator stopAnimating];
    if(networkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
       // NSLog(@"Locations=%@",jsonString);
        NSDictionary *jsonDict = [parser objectWithString:jsonString error:&error]; 
        if(error == nil)
        {
            NSString *successStatus = [jsonDict valueForKey:@"Success"];
            NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
            if([successStatus boolValue])
            {
                //success is true
                  if([errorMessage isEqualToString:@"No Error"])
                {
                    categoriesArray = [jsonDict valueForKey:@"Categories"];
                    [myTableView reloadData];
                    
                }
                else {
                    UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [errorMessageAlert show];
                }
            }
            else
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
        }
        else {
            //NSLog(@"get Locations Parsing Error");
        }
        
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [categoriesArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"CellIdentifier";
    UIView *view;
    UILabel *label;
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        view = [[UIView alloc]initWithFrame:CGRectMake(5, 2, 305, 48)];
        label = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, 250, 35)];
        label.tag = 2;
        view.tag = 1;
        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CategoryList"]];
        label.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:view];
        [cell.contentView addSubview:label];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        //  cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
        label.textColor = [UIColor blackColor];
        
  	}
    else {
        view = [cell.contentView viewWithTag:1];
        label = (UILabel *)[cell.contentView viewWithTag:2];
    }
    
   
    
   label.text =[[categoriesArray objectAtIndex:indexPath.row] valueForKey:@"CategoryName"];
   
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    StoreListViewController *cntrl = [[StoreListViewController alloc]initWithNibName:nil bundle:nil];
    cntrl.categoryId = [[categoriesArray objectAtIndex:indexPath.row] valueForKey:@"ID"];
    cntrl.marketId = marktetId;
    cntrl.navTitle = [[categoriesArray objectAtIndex:indexPath.row] valueForKey:@"CategoryName"];
    [self.navigationController pushViewController:cntrl
                                         animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - BackButton


-(IBAction)BakeClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
