//
//  UpdateListViewController.h
//  CityOnSale
//
//  Created by Jagsonics on 27/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"

@interface UpdateListViewController : SuperViewController
{
     NSString *storeId;
    IBOutlet UITextView *textView;
    IBOutlet UILabel *navTitle;
    IBOutlet UIButton *detailButton;
    NSString *storeName;
}

@property(nonatomic,retain) NSString *storeId;
@property(nonatomic,retain) NSString *storeName;
-(IBAction)storeDetailClicked:(id)sender;
@end
