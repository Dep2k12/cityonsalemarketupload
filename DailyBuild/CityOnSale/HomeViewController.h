//
//  HomeViewController.h
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"
#import  <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"


@interface HomeViewController : SuperViewController<UIPickerViewDelegate, UITabBarControllerDelegate>
{
    
  IBOutlet UITableView *locationTable;   
 // IBOutlet UIActivityIndicatorView *activityIndicator;
  NSArray *marketArray;  
  IBOutlet UILabel *marketLabel;

  IBOutlet UIPickerView *locationPick;
  NSArray *locationsArray;
  NSArray *locationsIDArray;
  NSString *locationID;
  NSString *locationString;
  NSString *Auothtoken;
  NSString *username;
  NSInteger getlocID;
  NSInteger rowInt;
    IBOutlet UIView *Cview;
    IBOutlet UIButton *CviewButton;
    IBOutlet UIButton *locationplaceholder;
}
-(void)fetchLocationMarket:(NSData *)responseData;
-(IBAction)LocstionPicker:(id)sender;
-(IBAction)done:(id)sender;



@end
