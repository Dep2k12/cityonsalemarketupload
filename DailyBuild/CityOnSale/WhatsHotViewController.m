//
//  WhatsHotViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WhatsHotViewController.h"


#import "SBJson.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UpdateListViewController.h"
@interface WhatsHotViewController ()

@end

@implementation WhatsHotViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.image = [UIImage imageNamed:@"HotDealsicon.png"];
        self.tabBarItem.title = @"What's Hot";
    }
    return self;
}


- (void)viewDidLoad
{
  
    
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    authToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
    userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    requestUrl = [NSString stringWithFormat:@"%@?username=%@&authToken=%@",getHotDealStoresUrl,userName,authToken];
    [super viewWillAppear:animated];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    NSString *storeId= [[storesArray objectAtIndex:indexPath.row]valueForKey:@"ID"];
    NSString *storeName= [[storesArray objectAtIndex:indexPath.row]valueForKey:@"StoreName"];
    UpdateListViewController *cntrl = [[UpdateListViewController alloc]initWithNibName:nil bundle:nil];
    cntrl.storeId = storeId;
    cntrl.storeName = storeName;
    
    [self.navigationController pushViewController:cntrl
                                         animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
