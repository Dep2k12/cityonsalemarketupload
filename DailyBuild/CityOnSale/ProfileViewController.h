//
//  ProfileViewController.h
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"

@interface ProfileViewController :SuperViewController{
    IBOutlet UIView *centerViewBG;
    IBOutlet UITextField *newPassword;
    IBOutlet UITextField *confirmNewPassword;
    IBOutlet UITextField *oldPassword;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *logoutBtnBgView;
    IBOutlet UIButton *unusedCouponsBtn;
    IBOutlet UIButton *logoutBtn;
    IBOutlet UIButton *changePasswordBtn;
    IBOutlet UIImageView *imageView;
    IBOutlet UILabel *navTitle;
    BOOL isChangePasswordView;
    BOOL isChangePasswordUser;
}
-(IBAction)changePasswordClicked;
-(IBAction)logoutClicked:(id)sender;
-(IBAction)showChangePasswordView:(id)sender;
-(IBAction)cancelButtonTapped:(id)sender;
@end
