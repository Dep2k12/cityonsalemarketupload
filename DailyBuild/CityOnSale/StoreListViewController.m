//
//  StoreListViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StoreListViewController.h"
#import "SBJson.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface StoreListViewController ()

@end

@implementation StoreListViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    authToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
    userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    requestUrl = [NSString stringWithFormat:@"%@?username=%@&marketID=%@&categoryID=%@&authToken=%@",getStoresUrl,userName,marketId,categoryId,authToken];
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
