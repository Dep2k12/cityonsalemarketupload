//
//  SearchViewController.h
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"
@interface SearchViewController : SuperViewController<UISearchBarDelegate>
{
    NSArray *storesArray;
    IBOutlet UITableView *searchTable;

}

@end
