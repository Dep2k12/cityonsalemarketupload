//
//  ProfileViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProfileViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "UnusedCouponsViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.image = [UIImage imageNamed:@"ProfileIcon.png"];
        self.tabBarItem.title = @"Profile";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isChangePasswordView = NO;
    isChangePasswordUser = NO;
    
    scrollView.contentSize = CGSizeMake(320, 500);
    //logoutBtnBgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"LoginBg"]];
    
    
    self.navigationController.navigationBarHidden=YES;
    centerViewBG.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pass-word-bg"]];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"MasterBg"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parseResponse:) name:@"ChangePasswordNotification" object:nil];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)parseResponse:(NSNotification *)notification
{
    id response = notification.object;
   // NSLog(@"response=%@",response);
    if( [[response valueForKey:@"PasswordChanged"] boolValue])
    {
        NSString *authToken = [response valueForKey:@"AuthToken"];
        [[NSUserDefaults standardUserDefaults] setObject:authToken forKey:@"Auothtoken"]; 
        [[NSUserDefaults standardUserDefaults]synchronize];
          UIAlertView *passwordChangeAlert = [[UIAlertView alloc]initWithTitle:@"City On Sale" message:@"PasswordChanged" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [passwordChangeAlert show];
    }
}

-(IBAction)editingChanged:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    
    if ([textField.text length] > 12) 
    {
        textField.text = [textField.text substringToIndex:12];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Maximum 12 characters allowed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    
}

-(IBAction)changePasswordClicked
{
    
    
    if((oldPassword.text.length > 0 ) && (newPassword.text.length > 0) && (confirmNewPassword.text.length > 0))
    {
        if([newPassword.text isEqualToString:confirmNewPassword.text])
        {
            //call the service
            //(string username, string oldPassword, string newPassword, string authtoken):
            authToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
            userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
            requestUrl = [NSString stringWithFormat:@"%@?username=%@&oldPassword=%@&newPassword=%@&authtoken=%@",changePasswordUrl,userName,oldPassword.text,newPassword.text,authToken];
            
          //  NSLog(@"Request Url=%@",requestUrl);
            [self callServiceWiithUrlString];
            serviceEndNotifName = @"ChangePasswordNotification";
            
            
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Password not changed" message:@"New password doesn't match with Confirm password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Password not changed" message:@"All fields are mandatory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    
}
-(IBAction)unUsedCoupons:(id)sender
{
    UnusedCouponsViewController *cntrl = [[UnusedCouponsViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:cntrl animated:YES];
    
}
-(IBAction)logoutClicked:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Are you sure you want to log out of City On Sale" message:nil delegate:self cancelButtonTitle:@"Confirm" otherButtonTitles:@"Cancel", nil];
    alert.tag = 3;
    [alert show];
    
    
}

-(IBAction)showChangePasswordView:(id)sender
{
    NSString *isStoreOwner = [[NSUserDefaults standardUserDefaults] valueForKey:@"isStoreOwner"];
    if([isStoreOwner boolValue])
    {
        isChangePasswordView = YES; 
    }
    else {
        isChangePasswordUser = YES;
    }
   
    navTitle.text = @"Change Password";
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:.6];
    logoutBtnBgView.center = CGPointMake(-300, self.view.center.y);
    [UIView commitAnimations];
    
    [UIView animateWithDuration:1.5 animations:^() {
        centerViewBG.alpha = 1;
    }];
    
    
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag ==3)
    {
        switch (buttonIndex) {
            case 0:
            {
                NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate showLogin];
               // [PFPush unsubscribeFromChannelInBackground:@""];
                
                
                [[NSUserDefaults standardUserDefaults] synchronize];   
            }
                break;  
        }
    }
        
        else
        {
            navTitle.text = @"Profile";
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:1];
            logoutBtnBgView.center = CGPointMake(self.view.center.x, self.view.center.y);
            [UIView commitAnimations];
            
            [UIView animateWithDuration:.5 animations:^() {
                centerViewBG.alpha = 0;
            }];
            
        }
        
    }
    
    -(IBAction)cancelButtonTapped:(id)sender
    {
        NSString *isStoreOwner = [[NSUserDefaults standardUserDefaults] valueForKey:@"isStoreOwner"];
        if([isStoreOwner boolValue])
        {
            isChangePasswordView = NO; 
        }
        else {
            isChangePasswordUser = NO;
        }
       
        navTitle.text = @"Profile";
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1];
        logoutBtnBgView.center = CGPointMake(self.view.center.x, self.view.center.y);
        [UIView commitAnimations];
        
        [UIView animateWithDuration:.5 animations:^() {
            centerViewBG.alpha = 0;
        }];
        newPassword.text = nil;
        oldPassword.text = nil;
        confirmNewPassword.text = nil;
    }
    
    
    - (void)viewDidUnload
    {
        [super viewDidUnload];
        // Release any retained subviews of the main view.
        // e.g. self.myOutlet = nil;
        
    }
    
    -(void)viewWillAppear:(BOOL)animated
    {
        [super viewWillAppear:animated];
        
        NSString *isStoreOwner = [[NSUserDefaults standardUserDefaults] valueForKey:@"isStoreOwner"];
        if([isStoreOwner boolValue])
        {
            if(!isChangePasswordView)
            {
                [unusedCouponsBtn setHidden:YES];
                logoutBtnBgView.frame = CGRectMake(27,145,266, 120);
                //logoutBtn.center = CGPointMake(logoutBtn.center.x, logoutBtn.center.y + 20);
                // changePasswordBtn.center = CGPointMake(changePasswordBtn.center.x, changePasswordBtn.center.y + 20);
                imageView.frame = CGRectMake(0,0,266, 120);
            }
            
        }
        else {
            
            if(!isChangePasswordUser)
            {
                [unusedCouponsBtn setHidden:NO];
                
                logoutBtnBgView.frame = CGRectMake(27,130,266, 160);;
                //logoutBtn.center = CGPointMake(logoutBtn.center.x, logoutBtn.center.y - 20);
                // changePasswordBtn.center = CGPointMake(changePasswordBtn.center.x, changePasswordBtn.center.y - 20); 
                imageView.frame = CGRectMake(0,0,266, 160);  
            }
            
        }
        
    }
    

    
    
    
    - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
    {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
    
    @end
