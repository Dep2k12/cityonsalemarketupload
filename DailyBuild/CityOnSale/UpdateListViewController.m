//
//  UpdateListViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 27/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UpdateListViewController.h"
#import "StoreListViewController.h"
#import "WhatsHotViewController.h"
#import "BestDealsViewController.h"
#import "StoreDetailViewController.h"

@interface UpdateListViewController ()

@end

@implementation UpdateListViewController
@synthesize storeId,storeName;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parseResponse:) name:@"UpdateNotification" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UpdateNotification" object:nil];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    navTitle.text = storeName;
   

    // Do any additional setup after loading the view from its nib.
    NSArray *cntrlsArray = [self.navigationController viewControllers];
    NSString *updataURL = nil;
    id previoiusVc = [cntrlsArray objectAtIndex:[cntrlsArray count]-2];
   // NSLog(@"PrevousVC=%@",previoiusVc);
    
      if([previoiusVc isKindOfClass:[StoreListViewController class]])
     {
     updataURL = getregularDealUpdatesUrl;
     
     }
     else if ([previoiusVc isKindOfClass:[WhatsHotViewController class]])
     {
     updataURL = getHotDealUpdatesUrl;
     }
     else {
     // from best deals
     updataURL = getBestDealUpdatesUrl;
     }
    authToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
    userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    requestUrl = [NSString stringWithFormat:@"%@?username=%@&storeID=%@&authToken=%@",updataURL,userName,storeId,authToken];
    
   // NSLog(@"Request Url=%@",requestUrl);
    [self callServiceWiithUrlString];
    serviceEndNotifName = @"UpdateNotification";

}

-(void)parseResponse:(NSNotification *)notification
{
    id response = notification.object;
   // NSLog(@"response=%@",response);
     NSArray *cntrlsArray = [self.navigationController viewControllers];
     id previoiusVc = [cntrlsArray objectAtIndex:[cntrlsArray count]-2];
     NSMutableString *bulletedUpdate = [[NSMutableString alloc]init];
     
     NSString *updateArrayKey = nil;
     NSString *updateTextKey = nil;
     
     if([previoiusVc isKindOfClass:[StoreListViewController class]])
     {
     updateArrayKey = @"RegularUpdates";
     updateTextKey = @"RegularUpdateText";
     
     }
     else if ([previoiusVc isKindOfClass:[WhatsHotViewController class]])
     {
     updateArrayKey = @"HotDealUpdates";
     updateTextKey = @"HotDealText";
     }
     else {
     // from best deals
     updateArrayKey = @"BestDealUpdates";
     updateTextKey = @"BestDealText";
     }

    
    NSArray *updateArray =  [response valueForKey:updateArrayKey];
    for(int i=1;i<=[updateArray count];i++)
    {
        [bulletedUpdate appendString:[NSString stringWithFormat:@">>  %@ \n\n",[[updateArray objectAtIndex:i-1]valueForKey:updateTextKey]]];
    }
    
    [textView setText:bulletedUpdate];
    [detailButton setEnabled:YES];
}

-(IBAction)storeDetailClicked:(id)sender
{
    StoreDetailViewController *cntrl = [[StoreDetailViewController alloc]initWithNibName:nil bundle:nil];
     cntrl.storeId = storeId;
    cntrl.storeName = storeName;
    [self.navigationController pushViewController:cntrl animated:YES];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
