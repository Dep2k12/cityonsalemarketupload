//
//  RegisterViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#define SCROLLVIEW_CONTENT_HEIGHT 860
#define SCROLLVIEW_CONTENT_WIDTH  820
#define MAXLENGTH 12

#import "RegisterViewController.h"
#import  "SBJson.h"
#import "AppDelegate.h"

#import "UIView+FindAndResignFirstResponder.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        locationPick = nil;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
 
     existFlage=1;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    regScroll.contentSize = CGSizeMake(320, 600);
        
    rowInt=0;
    locationsArray=[[NSArray alloc]init];
    // Do any additional setup after loading the view from its nib.
   //Cview.alpha=0;
    //CviewButton.alpha=0;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"MasterBg"]];
    registerBgView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"RegisterBg"]];
    
    dispatch_async(kBgQueue, ^{
        
        
        requestUrl = getAllLocations;    
        
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:  requestUrl] options:0 error:&error];
        networkError = error;
       [ self performSelectorOnMainThread:@selector(fetchedLocationsData:) 
                                   withObject:data waitUntilDone:YES]; 
       
    });    
    
}

-(void)fetchedLocationsData:(NSData *)responseData
{
    [activityIndicator stopAnimating];
    if(networkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        //NSLog(@"Locations=%@",jsonString);
        NSDictionary *jsonDict = [parser objectWithString:jsonString error:&error]; 
        if(error == nil)
        {
            NSString *successStatus = [jsonDict valueForKey:@"Success"];
            NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
            if([successStatus boolValue])
            {
                //success is true
                  if([errorMessage isEqualToString:@"No Error"])
                {
                    // there is no error message
                    NSDictionary *locDic = [jsonDict objectForKey:@"Locations"];
                    //NSLog(@"locationsArray=%@",locDic);
                    locationsArray=[locDic valueForKey:@"LocationName"];  
                    locationsIDArray=[locDic valueForKey:@"ID"];
                   // NSLog(@"locationsArray=%@",locationsArray);
                    
                    if([locationsArray count] > 0)
                    {
                        locationLabel.text = [locationsArray objectAtIndex:0];
                        locationID = [locationsIDArray objectAtIndex:0];
                    }
                }
                else {
                    UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [errorMessageAlert show];
                }
            }
            else
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
        }
        else {
           // NSLog(@"get Locations Parsing Error");
        }
        
        
    }
    
}

-(IBAction)editingChanged:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    if(textField.tag == 1)
    {
        if (textField.text.length > 25) 
        {
            textField.text = [textField.text substringToIndex:25];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Maximum 25 characters allowed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            
        }
        
    }
    if(textField.tag == 2)
    {
        if (textField.text.length > 25) 
        {
            textField.text = [textField.text substringToIndex:25];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Maximum 25 characters allowed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            
        }
        
    }
    if(textField.tag == 3)
    {
        if (textField.text.length > 254) 
        {
            textField.text = [textField.text substringToIndex:254];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Maximum 254 characters allowed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            
        }
        
    }


    if(textField.tag == 4)
    {
        if (textField.text.length > 12) 
        {
            textField.text = [textField.text substringToIndex:12];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Maximum 12 characters allowed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            
        }
        
    }
    if(textField.tag == 5)
    {
       // NSLog(@"length=%d",textField.text.length);
        if (textField.text.length >15) 
        {
            textField.text = [textField.text substringToIndex:15];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Maximum 15 characters allowed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
             
        }
        
    }
    

}


-(IBAction)editingStarted:(id)sender
{
    // shift the view up
     if (!isShiftedUp)
     {
     
     [UIView beginAnimations:nil context:NULL];
     [UIView setAnimationDuration:.5];
     registerBgView.center = CGPointMake(registerBgView.center.x, registerBgView.center.y - 100);
     [UIView commitAnimations];
     isShiftedUp = YES;
     }
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:nil]; 
    
  
    keyboardHeadView = [[UIView alloc]initWithFrame:CGRectMake(-320, 200, 320, 45)];
    keyboardHeadView.backgroundColor = [UIColor lightGrayColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(240,5,70,35);
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    
    button.backgroundColor = [UIColor darkGrayColor];
    button.layer.cornerRadius = 5;
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 1;
    
    [button setTitle:@"Done" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(dismissKeyboard:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [keyboardHeadView addSubview:button];
    
    
    keyboardHeadView.alpha = 0.0;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];  
    keyboardHeadView.alpha = .9;
    keyboardHeadView.frame = CGRectMake(0,200 ,320,45);
    [self.view addSubview:keyboardHeadView];

    
    [UIView commitAnimations];
}
- (void)keyboardWillHide:(NSNotification *)note {
    //hide button here
    [keyboardHeadView removeFromSuperview];
    
}
-(IBAction)editingEnded:(id)sender
{
    
    // shift the view down
   /* if (isShiftedUp)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.5];
        registerBgView.center = CGPointMake(registerBgView.center.x, registerBgView.center.y + 100);
        [UIView commitAnimations]; 
        isShiftedUp = NO;
    }*/
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillShowNotification 
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];
    
    [keyboardHeadView setHidden:YES];

   
    
}

- (void)keyboardWillShow:(NSNotification *)note {
    //Get view that's controlling the keyboard
   
 
}

-(void)dismissKeyboard:(id)sender
{
    
    [self.view findAndResignFirstResponder];

    [keyboardHeadView setHidden:YES];
    
    
    if (isShiftedUp)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.5];
        registerBgView.center = CGPointMake(registerBgView.center.x, registerBgView.center.y + 100);
        [UIView commitAnimations]; 
        isShiftedUp = NO;
    }

    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillShowNotification 
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];
    
    [keyboardHeadView setHidden:YES];

}


- (void)viewDidUnload
{
    [super viewDidUnload];

}

#pragma mark - IBActions

-(IBAction)returnKeyPressed:(id)sender
{
    [super returnKeyPressed:sender];
    [locationPick setHidden:YES];
    [Cview setHidden:YES];
}

-(IBAction)resetKeyPressed:(id)sender
{
   
    existFlage=1;
    firstNameField.text = @"";
    lastNameField.text = @"";
    emailField.text = @"";
    passwordField.text = @"";
    phoneNoField.text = @"";
    locationBtn.titleLabel.text = @"";
     locationLabel.text=@"Select";
}
-(IBAction)registerButtonPressed:(id)sender 
{
    
    registerBTN.enabled=NO;
    
  //  NSLog(@"%@",locationLabel.text);
   
    if(existFlage==0)
    {
        registerBTN.enabled=YES;
        UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:@"Email already registered " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [errorMessageAlert show];
        
    }
    else
    {
        if((emailField.text.length > 0) && (passwordField.text.length > 0) &&
               (firstNameField.text.length > 0) && (lastNameField.text.length > 0) &&(![locationLabel.text isEqualToString:@"Select"]))
        {
        
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
        if (![emailTest evaluateWithObject:emailField.text])
        {
           registerBTN.enabled=YES;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Not a valid email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else 
        {
            [activityIndicator startAnimating];
              dispatch_async(kBgQueue, ^{
                
                
         requestUrl = [NSString stringWithFormat:@"http://www.cityonsale.in/User/Register?firstName=%@&lastName=%@&email=%@&password=%@&phoneNumber=%@&locationID=%@",firstNameField.text,lastNameField.text,emailField.text,passwordField.text,phoneNoField.text,locationID];    
                  
                  NSLog(@"%@",requestUrl);
                  
                   requestUrl=[requestUrl stringByReplacingOccurrencesOfString:@" " withString:@"+"];
                  
                  
                
                NSError *error = nil;
                NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:requestUrl]];
                networkError = error;
                [ self performSelectorOnMainThread:@selector(fetchedregisterresponse:) 
                                        withObject:data waitUntilDone:YES]; 
                
            });    
     }
        
        
    }
    else {
        registerBTN.enabled=YES;
        UIAlertView *validationAlert = [[UIAlertView alloc]initWithTitle:@"Registration failure" message:@"Fields marked with * are required" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [validationAlert show];
        [activityIndicator stopAnimating];
    }
    }
    
}

-(void)fetchedregisterresponse:(NSData *)responseData
{
   
    
    [activityIndicator stopAnimating];
    
    if([responseData length]==0)
    {
        existFlage=1;
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Server Error" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
   else
   {   
    if(networkError != nil)
    {
        existFlage=1;
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    
        else {
            NSError *error = nil;
            SBJsonParser *parser=[[SBJsonParser alloc] init];
            
            NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
          //  NSLog(@"Locations=%@",jsonString);
            NSDictionary *jsonDict = [parser objectWithString:jsonString error:&error]; 
            if(error == nil)
            {
                NSString *successStatus = [jsonDict valueForKey:@"Success"];
                NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
                
                if([successStatus boolValue])
                {
                 //success is true
                    if([errorMessage isEqualToString:@"No Error"])                 {
                   // there is no error message
                   auothtoken = [jsonDict valueForKey:@"AuthToken"];
                 //  NSLog(@"auothtoken=%@",auothtoken);
                     
                     
                    
              
                        [[NSUserDefaults standardUserDefaults] setValue:locationLabel.text forKey:@"dropDownText"];
                        [[NSUserDefaults standardUserDefaults] setValue:locationID forKey:@"locID"];
                      
                    
                     [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"isLogin"];
                     [[NSUserDefaults standardUserDefaults] setObject:auothtoken forKey:@"Auothtoken"];
                    
                     [[NSUserDefaults standardUserDefaults] setObject:emailField.text forKey:@"username"];
                     //[[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"RememberMe"];
                     [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"RememberMe"];
                                         
                          [[NSUserDefaults standardUserDefaults]synchronize];
                     [self dismissModalViewControllerAnimated:YES];
                     [_appDeleg hideLogin];
                   
                  }
                    else {
                        UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [errorMessageAlert show];
                    }
                }
                else
                {
                    UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [successAlert show];
                }
            }
            else {
               /// NSLog(@"get Locations Parsing Error");
            }
            
            
        }
   }
}

-(IBAction)DisplayRound:(id)sender
{
   
    

    
    [firstNameField resignFirstResponder];
    [lastNameField resignFirstResponder];
    [emailField resignFirstResponder];
    [passwordField resignFirstResponder];
    [phoneNoField resignFirstResponder];
    

          
   
    if((locationsArray != nil) && ([locationsArray count] > 0))

    {
        
        if(locationPick == nil)
        {
            locationPick = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 280, 320, 200)];
           // lo
            locationPick.delegate = self;
            locationPick.showsSelectionIndicator = YES;
             [self.view addSubview:locationPick];
            
        }

        if(rowInt==0)
        {
            [locationPick selectRow:0 inComponent:0 animated:YES];
            [self pickerView:locationPick didSelectRow:0 inComponent:0];
            
        }
        else 
        {
            [locationPick selectRow:rowInt inComponent:0 animated:YES];
            
        }
        
        [locationPick setHidden:NO];
        [Cview setHidden:NO];
        
        if (!isShiftedUp)
        {
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:.5];
            registerBgView.center = CGPointMake(registerBgView.center.x, registerBgView.center.y - 100);
            [UIView commitAnimations];
            isShiftedUp = YES;
        }
        
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:@"No locations found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    

   // Cview.alpha=1;
   // CviewButton.alpha=1;
    
   
    
}



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [locationsArray count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [locationsArray objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    //NSLog(@"Selected Color: %@. Index of selected color: %i", [locationsArray objectAtIndex:row], row);
    rowInt=row;
    locationLabel.text = [locationsArray objectAtIndex:row];
   // registerBTN.titleLabel.text=[locationsArray objectAtIndex:row];
    locationID=[locationsIDArray objectAtIndex:row];

}
// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 320;
     return sectionWidth;
}

-(IBAction)touchOUtside:(id)sender
{
    // locationPick.hidden = YES;
}

-(IBAction)done:(id)sender
{
    //Cview.alpha=0;
   // CviewButton.alpha=0;
    locationPick.hidden = YES;
    
    [Cview setHidden:YES];
    if (isShiftedUp)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.5];
        registerBgView.center = CGPointMake(registerBgView.center.x, registerBgView.center.y + 100);
        [UIView commitAnimations]; 
        isShiftedUp = NO;
    }
    
    
    
}



-(IBAction)ValidateUser
{
    
    
    //regScroll.contentSize=CG
       
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
    if (![emailTest evaluateWithObject:emailField.text])
    {
        existFlage=1;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Not a valid email address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        [activityIndicator startAnimating];
        dispatch_async(kBgQueue, ^{
        // NSString *requestUrl = getAllLocations;    
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://www.cityonsale.in/User/UsernameExists?email=%@",emailField.text]]];
        networkError = error;
        [ self performSelectorOnMainThread:@selector(FetchUserExitingresponse:) 
         withObject:data waitUntilDone:YES]; 
        
    });      
}
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [[self.view viewWithTag:3] becomeFirstResponder];
}
-(void)FetchUserExitingresponse:(NSData*)responseData
{
    
    [activityIndicator stopAnimating];
    
    if(networkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
       // NSLog(@"Locations=%@",jsonString);
        NSDictionary *jsonDict = [parser objectWithString:jsonString error:&error]; 
        if(error == nil)
        {
            NSString *successStatus = [jsonDict valueForKey:@"Success"];
            NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
            
            if([successStatus boolValue])
            {
                //success is true
                  if([errorMessage isEqualToString:@"No Error"])                {
                    // there is no error message
                    
                    NSString *UserExists=[jsonDict valueForKey:@"UserExists"];
                    
                    if([UserExists boolValue])
                    {
                        existFlage=0;
                        UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:@"Email already registered " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [errorMessageAlert show];
                    }
                    else 
                    {
                       existFlage=1;   
                        [[self.view viewWithTag:4] becomeFirstResponder];
                    }
                                                  
                }
                else {
                    UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [errorMessageAlert show];
                }
            }
            else
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
        }
        else {
          //  NSLog(@"get Locations Parsing Error");
     }
 }

}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(IBAction)backButtonClick:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
