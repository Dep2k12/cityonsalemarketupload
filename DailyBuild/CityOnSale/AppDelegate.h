//
//  AppDelegate.h
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "SearchViewController.h"
#import "BestDealsViewController.h"
#import "WhatsHotViewController.h"
#import "ProfileViewController.h"
#import "LoginViewController.h"


#define _appDeleg ((AppDelegate *)[[UIApplication sharedApplication] delegate])
@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>
{
    UINavigationController *navigationControllerHome,*navigationControllerSearch,*navigationControllerBestDeals, *navigationControllerWhatsHot,*navigationControllerProfile,*navigationControllerStoreOwner;  
    LoginViewController *_loginView;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *tabBarController;
-(void)showLogin;
-(void)hideLogin;
@end
