//
//  StoreOwnerViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 26/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StoreOwnerViewController.h"
#import  "AppDelegate.h"

@interface StoreOwnerViewController ()

@end

@implementation StoreOwnerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.tabBarItem.image = [UIImage imageNamed:@"HomeIcon.png"];
        self.tabBarItem.title = @"Home";
        

    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setHidden:YES];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"MasterBg"] ];
    bgView.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"blank-documentsbg"] ];

    scrollView.contentSize = CGSizeMake(320,550);
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parseResponse:) name:@"RedeemPointsNotification" object:nil];
    
    //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    


}

-(IBAction)submitClicked:(id)sender
{
    //(string username, string couponCode, string authToken)
   // NSLog(@"codeTExt=%@",codeField.text);
    if(codeField.text.length > 0)
    {
        authToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
        userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
        requestUrl = [NSString stringWithFormat:@"%@?username=%@&couponCode=%@&authtoken=%@",RedeemCouponCodeUrl,userName,codeField.text,authToken];
        
       // NSLog(@"Request Url=%@",requestUrl);
        [self callServiceWiithUrlString];
        serviceEndNotifName = @"RedeemPointsNotification";
    }
    else {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:@"Missing required field" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    
}

-(void)parseResponse:(NSNotification *)notification
{
    id response = notification.object;
   // NSLog(@"response=%@",response);
        if([[response valueForKey:@"CouponRedeemed"] boolValue])
        {
            NSString *storeOffering = [response valueForKey:@"StoreOffering"];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Discount offered " message:storeOffering delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
//    if( [[response valueForKey:@"PasswordChanged"] boolValue])
//    {
//        UIAlertView *passwordChangeAlert = [[UIAlertView alloc]initWithTitle:@"City On Sale" message:@"PasswordChanged" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [passwordChangeAlert show];
//    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    id nextVC = [(UINavigationController *)viewController topViewController];
    id currentVC = [(UINavigationController *)tabBarController.selectedViewController topViewController];
    return (nextVC != currentVC);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
