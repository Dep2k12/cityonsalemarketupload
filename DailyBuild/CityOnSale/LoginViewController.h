//
//  LoginViewController.h
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"

@interface LoginViewController : SuperViewController<UITextFieldDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *loginBgView;
    IBOutlet UIButton *checkBoxBtn;
    IBOutlet UITextField *userNameField;
    IBOutlet UITextField *passwordField;
    IBOutlet UIActivityIndicatorView *activityindicator;
    NSString *username;
    NSString *password;
    NSString *auothtoken;
    UIAlertView *alertTextView;
    
}

-(IBAction)returnKeyPressed:(id)sender;
-(IBAction)checkBoxClicked:(id)sender;
-(IBAction)registerUserPressed:(id)sender;
-(IBAction)loginBtnPressed:(id)sender;
-(IBAction)forgotPassword:(id)sender;

-(void)LoginResponse:(NSData *)responseData;


@end
