//
//  SubcategoryViewController.h
//  CityOnSale
//
//  Created by APPLE Mac Mini on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SuperViewController.h"
#import "StoreListViewController.h"

@interface SubcategoryViewController : SuperViewController<UITableViewDataSource,UITableViewDelegate>
{
  //  IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UITableView *myTableView;
    NSArray *categoriesArray;
    NSString *marktetId;
}
-(IBAction)BakeClick:(id)sender;


@property (nonatomic,strong) NSString *marktetId;
@end
