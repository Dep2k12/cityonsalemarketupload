//
//  UnusedCouponsViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UnusedCouponsViewController.h"
#import "NSData+NSDataAdditions.h"

@interface UnusedCouponsViewController ()

@end

@implementation UnusedCouponsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parseResponse:) name:@"unUsedCouponsNotification" object:nil];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    //(string username, string oldPassword, string newPassword, string authtoken):
    authToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
    userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    requestUrl = [NSString stringWithFormat:@"%@?username=%@&authtoken=%@",GetUnusedCouponCodesUrl,userName,authToken];
    
   // NSLog(@"Request Url=%@",requestUrl);
    [self callServiceWiithUrlString];
    serviceEndNotifName = @"unUsedCouponsNotification";

    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
       
    
    // Do any additional setup after loading the view from its nib.
}

-(void)parseResponse:(NSNotification *)notification
{
    id response = notification.object;
   // NSLog(@"response=%@",response);
    
    couponsCodesArray = [response valueForKey:@"CouponCodes"];
    [myTableView reloadData];
    if([couponsCodesArray count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:@"No coupons found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }

}

#pragma mark - tableMethods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [couponsCodesArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"CellIdentifier";
    UIImageView *logoImage;
    UILabel *titleLabel;
    UILabel *subtitleLabel;
    UILabel *storeNameLabel;
  
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
        
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
        
        /*cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
        
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        cell.detailTextLabel.numberOfLines = 2;
        cell.detailTextLabel.textColor = [UIColor blackColor];*/
        
        logoImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, 50, 50)];
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70,10, 250, 20)];
        storeNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 30, 250,15)];
        subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 45, 250, 30)];
        
        
        logoImage.tag = 2;
        titleLabel.tag = 3;
        subtitleLabel.tag = 4;
        storeNameLabel.tag = 5;
        
        
        subtitleLabel.font = [UIFont systemFontOfSize:12];
        subtitleLabel.numberOfLines = 2;
        subtitleLabel.lineBreakMode = UILineBreakModeWordWrap;
        storeNameLabel.font = [UIFont systemFontOfSize:12];
        
    
        
        [cell.contentView addSubview:logoImage];
        [cell.contentView addSubview:titleLabel];
        [cell.contentView addSubview:subtitleLabel];
        [cell.contentView addSubview:storeNameLabel];
  	}
    else {
        logoImage = (UIImageView *)[cell.contentView viewWithTag:2];
        titleLabel = (UILabel *)[cell.contentView viewWithTag:3];
        subtitleLabel = (UILabel *)[cell.contentView viewWithTag:4];
        storeNameLabel = (UILabel *)[cell.contentView viewWithTag:5];
    }
    NSString *storeLogoData = [[couponsCodesArray objectAtIndex:indexPath.row] valueForKey:@"StoreLogo"];

    NSString *couponCode = [[couponsCodesArray objectAtIndex:indexPath.row] valueForKey:@"CouponCode"];
    NSString *storeName = [[couponsCodesArray objectAtIndex:indexPath.row] valueForKey:@"StoreName"];

   // NSString *dateCreated  = [[couponsCodesArray objectAtIndex:indexPath.row] valueForKey:@"DateCreated"];
    
     NSString *storeAddress  = [[couponsCodesArray objectAtIndex:indexPath.row] valueForKey:@"StoreAddress"];
    
    
    if(storeLogoData != (id)[NSNull null])
    {
        NSData *imageData = [NSData decodeBase64WithString:storeLogoData];
        
        logoImage.image = ([UIImage imageWithData:imageData]);  
    }
    else {
        logoImage.image = [UIImage imageNamed:@"No-Image-Available.jpg"];
    }
   
    titleLabel.text =( couponCode == (id)[NSNull null] ? @"No Data" : [NSString stringWithFormat:@"Coupon Code: %@", couponCode]);
    subtitleLabel.text = (couponCode == (id)[NSNull null] ? @"No Data" : [NSString stringWithFormat:@"Store Address: %@",storeAddress]);
    storeNameLabel.text = (storeName == (id)[NSNull null] ? @"No Data" : [NSString stringWithFormat:@"Store Name: %@",storeName]);
    
    return cell;
 
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
