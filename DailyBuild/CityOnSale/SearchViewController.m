//
//  SearchViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchViewController.h"
#import "StoreListCell.h"
#import "StoreDetailViewController.h"
#import "NSData+NSDataAdditions.h"
@interface SearchViewController ()

@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.image = [UIImage imageNamed:@"SearchIcon.png"];
        self.tabBarItem.title = @"Search";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"MasterBg"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parseResponse:) name:@"searchNotification" object:nil];
    
    UISearchBar *searchBar= [[UISearchBar alloc] initWithFrame:
                       CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)] ; 
    searchBar.tag = 2;
    //searchBar.backgroundColor = [UIColor lightGrayColor];
    searchTable.tableHeaderView = searchBar;
    searchBar.delegate = self;
    searchBar.showsCancelButton = YES;
    for( UIView *subview in searchBar.subviews )
    {
        if( [subview isKindOfClass:NSClassFromString( @"UISearchBarBackground" )] )
        {
            UIView *bg = [[UIView alloc] initWithFrame:subview.frame];
            
            bg.backgroundColor = [UIColor colorWithRed:.1 green:.1 blue:.1 alpha:.2];
           
            [searchBar insertSubview:bg aboveSubview:subview];
            
            [subview removeFromSuperview];
        
            break;

        }
    }
    

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
  //  NSLog(@"SearchButton Clicked") ;
    [searchBar resignFirstResponder];
    
    authToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
    userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    NSString *searchText = searchBar.text;
    
    NSString *trimmedText= [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(trimmedText.length == 0)
    {
        searchBar.text = @"";
        
    }
    else {
       // [sender resignFirstResponder];
        requestUrl = [NSString stringWithFormat:@"%@?username=%@&searchText=%@&authtoken=%@",SearchUrl,userName,trimmedText,authToken];
        
     //   NSLog(@"Request Url=%@",requestUrl);
        [self callServiceWiithUrlString];
        serviceEndNotifName = @"searchNotification";
        
    }    

}// called when keyboard search button pressed
- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
{
   // NSLog(@"SearchButton Clicked") ;

}
// called when bookmark button pressed
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBar.text = @"";
    [[self.view viewWithTag:2] resignFirstResponder];

 
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
   // NSLog(@"CancelhButton Clicked") ;

}// called when text ends editing


-(void)parseResponse:(NSNotification *)notification
{
    id response = notification.object;
   // NSLog(@"response=%@",response);
    storesArray = [response valueForKey:@"Stores"];
    
    [searchTable reloadData];
    if([storesArray count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"City On Sale" message:@"Store not found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [[self.view viewWithTag:2] resignFirstResponder];
//}

#pragma mark - tableMethods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [storesArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"CellIdentifier";
    
	StoreListCell *storeListCell = (StoreListCell *)[tableView 
                                                     dequeueReusableCellWithIdentifier: cellIdentifier];
    if (storeListCell == nil)  
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StoreListCell" owner:self options:nil];
        
        for (id oneObject in nib)
            if ([oneObject isKindOfClass:[StoreListCell class]])
                storeListCell = (StoreListCell *)oneObject;
        storeListCell.selectionStyle = UITableViewCellSelectionStyleNone;
        storeListCell.storeBgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"store-rowbg"]];
    }    
    
    NSString *storeName = [[storesArray objectAtIndex:indexPath.row]valueForKey:@"StoreName"];
    NSString *storeType = [[storesArray objectAtIndex:indexPath.row] valueForKey:@"TypeName"];
    NSString *storeDiscount = [[storesArray objectAtIndex:indexPath.row]valueForKey:@"StoreOffering"];
    NSString *storePrice=[[storesArray objectAtIndex:indexPath.row]valueForKey:@"StorePrice"];
    
    
    NSString *storeLogoData = [[storesArray objectAtIndex:indexPath.row]valueForKey:@"StoreLogo"];
    
    storeListCell.storeName.text =   ((storeName == (id)[NSNull null]) ? @"No Data" : storeName);
    storeListCell.storeDiscount.text = ((storeDiscount == (id)[NSNull null]) ? @"No Data" :  [NSString stringWithFormat:@"%@  Offered",storeDiscount ]);
    storeListCell.storePriceRange.text = ((storePrice == (id)[NSNull null]) ? @"No Data" : [NSString stringWithFormat:@"Price Range: %@",storePrice ]); 
    storeListCell.storeType.text = ((storeType == (id)[NSNull null]) ? @"No Data" : storeType);
    if(storeLogoData != (id)[NSNull null])
    {
        NSData *imageData = [NSData decodeBase64WithString:storeLogoData];
        [storeListCell.storeLogo setImage:[UIImage imageWithData:imageData]];
    }
    else {
        [storeListCell.storeLogo setImage:[UIImage imageNamed:@"No-Image-Available.jpg"]];
    }
    
    return storeListCell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    NSString *storeId= [[storesArray objectAtIndex:indexPath.row]valueForKey:@"ID"];
    NSString *storeName= [[storesArray objectAtIndex:indexPath.row]valueForKey:@"StoreName"];
    
    StoreDetailViewController *cntrl = [[StoreDetailViewController alloc]initWithNibName:nil bundle:nil]; 
    
    cntrl.storeId = storeId;
    cntrl.storeName = storeName;
    
    [self.navigationController pushViewController:cntrl
                                         animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
