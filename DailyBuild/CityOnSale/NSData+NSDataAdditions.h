//
//  NSData+NSDataAdditions.h
//  SyncMa
//
//  Created by  Abhi on 3/22/12.
//  Copyright (c) 2012   Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSString;

@interface NSData (NSDataAdditions)

+ (NSData *) base64DataFromString:(NSString *)string;

+ (NSData *)decodeBase64WithString:(NSString *)strBase64;
@end
