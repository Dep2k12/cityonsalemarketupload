//
//  HomeViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "AppDelegate.h"
#import  "SBJson.h"
#import "SubcategoryViewController.h"
#import "StoreOwnerViewController.h"

@interface HomeViewController ()
@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.image = [UIImage imageNamed:@"HomeIcon.png"];
        self.tabBarItem.title = @"Home";
        
    }
    return self;
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    Cview.alpha=0;
    CviewButton.alpha=0;
    
       
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated]; 
    self.navigationController.navigationBarHidden=YES;
  
    NSString *locName;
    
    [activityIndicator startAnimating];
    
    Auothtoken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
    getlocID=[[[NSUserDefaults standardUserDefaults] valueForKey:@"locID"] intValue];
    NSLog(@"locationId=%d",getlocID);
    locName= [[NSUserDefaults standardUserDefaults] valueForKey:@"isLocation"];
    if(locName==nil)
    {
        
        locName =@"Select Location";
    }
    
    
    username= [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    // marketLabel.text=locName;
    
    dispatch_async(kBgQueue, ^{
        
        requestUrl = getAllLocations;    
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:  requestUrl] options:0 error:&error];
        networkError = error;
        [ self performSelectorOnMainThread:@selector(fetchedLocationsData:) 
                                withObject:data waitUntilDone:YES]; 
        
    });    
    
    
    

}
-(void)fetchedLocationsData:(NSData *)responseData
{
    [activityIndicator stopAnimating];
    if(networkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
       // NSLog(@"Locations=%@",jsonString);
        NSDictionary *jsonDict = [parser objectWithString:jsonString error:&error]; 
        if(error == nil)
        {
            NSString *successStatus = [jsonDict valueForKey:@"Success"];
            NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
            if([successStatus boolValue])
            {
                //success is true
                if([errorMessage isEqualToString:@"No Error"])
                {
                    // there is no error message
                    
                    NSDictionary *locDic = [jsonDict objectForKey:@"Locations"];
                   NSLog(@"locationsdictArray=%@",locDic);    // id and names
                    locationsArray=[locDic valueForKey:@"LocationName"];  
                    locationsIDArray=[locDic valueForKey:@"ID"];
                    NSLog(@"locationsNamesArray=%@",locationsArray);   // only names of citites
                    
                    
                    
                    
                    username= [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
                    Auothtoken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
                    locationID = [[NSUserDefaults standardUserDefaults] valueForKey:@"locID"];
                    if(locationID !=nil)
                    {
                        
                        
                        marketLabel.text =  [[NSUserDefaults standardUserDefaults]valueForKey:@"dropDownText"];
                        
                        
                        
                    }
                    else {
                        locationID =[locationsIDArray objectAtIndex:0];
                        marketLabel.text = [locationsArray objectAtIndex:0];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:locationID forKey:@"locID"];
                        [[NSUserDefaults standardUserDefaults] setObject:marketLabel.text forKey:@"dropDownText"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    NSString *URLStr=[NSString stringWithFormat:@"http://www.cityonsale.in/Store/GetAllMarkets?username=%@&locationID=%d&authToken=%@",username,[locationID integerValue],Auothtoken];
                    
                   NSLog(@"%@",URLStr);
                    [activityIndicator startAnimating];
                    dispatch_async(kBgQueue, ^{
                        NSError *error = nil;
                        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:URLStr]];
                        networkError = error;
                        [ self performSelectorOnMainThread:@selector(fetchLocationMarket:) 
                                                withObject:data waitUntilDone:YES]; 
                        
                    });  
                    
                    
                    
                }
                else {
                    UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [errorMessageAlert show];
                }
            }
            else
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
        }
        else {
           // NSLog(@"get Locations Parsing Error");
        }
        
        
    }
}
-(void)fetchLocationMarket:(NSData *)responseData;
{     
    
    [activityIndicator stopAnimating];
    if(networkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"Marketss=%@",jsonString);
        NSDictionary *jsonDict = [parser objectWithString:jsonString error:&error]; 
        if(error == nil)
        {
            NSString *successStatus = [jsonDict valueForKey:@"Success"];
            NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
            if([successStatus boolValue])
            {
                //success is true
                if([errorMessage isEqualToString:@"No Error"])
                {
                    // there is no error message
                    marketArray = [jsonDict valueForKey:@"Markets"];
                    NSLog(@"marketsArray=%@",marketArray);
                    [locationTable reloadData];
                    
                    //an array of dict(MarketName,ID)
                    
                    
                }
                else {
                    UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [errorMessageAlert show];
                }
            }
            else
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
        }
        else {
            //NSLog(@"get Locations Parsing Error");
        }
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [marketArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CellIdentifier = @"CellIdentifier";
    UIView *view;
    UILabel *label;
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        view = [[UIView alloc]initWithFrame:CGRectMake(5, 2, 305, 48)];
        label = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, 250, 35)];
        label.tag = 2;
        view.tag = 1;
        view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CategoryList"]];
        label.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:view];
        [cell.contentView addSubview:label];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        //  cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
        label.textColor = [UIColor blackColor];
        
  	}
    else {
        view = [cell.contentView viewWithTag:1];
        label = (UILabel *)[cell.contentView viewWithTag:2];
    }
    
    
    label.text =[[marketArray objectAtIndex:indexPath.row]valueForKey:@"MarketName"];
    
    
    
    return cell;   
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SubcategoryViewController *dvController = [[SubcategoryViewController alloc] initWithNibName:@"SubcategoryViewController" bundle:[NSBundle mainBundle]];
    // dvController.selectedCountry = selectedCountry;
    dvController.marktetId =  [[marketArray objectAtIndex:indexPath.row]valueForKey:@"ID"];
    
    [self.navigationController pushViewController:dvController animated:YES];
    dvController = nil;
}








-(IBAction)LocstionPicker:(id)sender
{
    locationplaceholder.enabled=NO;
    Cview.alpha=1;
    CviewButton.alpha=1;
    locationPick = [[UIPickerView alloc] initWithFrame:CGRectMake(0,200, 320,216)];
    locationPick.delegate = self;
    locationPick.showsSelectionIndicator = YES;
    
    if(rowInt==0)
    {
        [locationPick selectRow:0 inComponent:0 animated:YES];
        [self pickerView:locationPick didSelectRow:0 inComponent:0];
        
    }
    else 
    {
        [locationPick selectRow:rowInt inComponent:0 animated:YES];
        
    }
    
    
    [self.view addSubview:locationPick];
}



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [locationsArray count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [locationsArray objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    
   // NSLog(@"Selected Color: %@. Index of selected color: %i", [locationsArray objectAtIndex:row], row);
    rowInt=row;
    marketLabel.text=[locationsArray objectAtIndex:row];
    locationID=[locationsIDArray objectAtIndex:row]; 
    
    [[NSUserDefaults standardUserDefaults] setValue:marketLabel.text forKey:@"dropDownText"];
    [[NSUserDefaults standardUserDefaults] setValue:locationID forKey:@"locID"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

-(IBAction)done:(id)sender
{
    
    [[NSUserDefaults standardUserDefaults] setValue:marketLabel.text forKey:@"dropDownText"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    Cview.alpha=0;
    CviewButton.alpha=0;
    locationPick.hidden = YES;
    locationplaceholder.enabled=YES;
    
    [activityIndicator startAnimating];
    NSString *URLStr=[NSString stringWithFormat:@"http:/www.cityonsale.in/Store/GetAllMarkets?username=%@&locationID=%d&authToken=%@",username,[locationID integerValue],Auothtoken];
    
  //  NSLog(@"%@",URLStr);
    
    
    dispatch_async(kBgQueue, ^{
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:URLStr]];
        networkError = error;
        [ self performSelectorOnMainThread:@selector(fetchLocationMarket:) 
                                withObject:data waitUntilDone:YES]; 
        
    });  
    
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
