//
//  LoginViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import  "SBJson.h"
#define MAXLENGTH 12
#define TAG_TEXT_ALERT 5

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - view LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parseResponse:) name:@"ForgotPasswordNotification" object:nil];
    
     [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"RememberMe"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    scrollView.contentSize = CGSizeMake(320, 550);
    [activityindicator stopAnimating];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"MasterBg"]] ;
    loginBgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"LoginBg"]] ;
    
    
    alertTextView= [[UIAlertView alloc]initWithTitle:@"Please provide your registered email \n \n" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [alertTextView setFrame:CGRectMake(20, 250, 280, 250)];
    
    
    UITextField * alertTextField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 65.0, 260.0, 30.0)];
    [alertTextField setBackgroundColor:[UIColor whiteColor]];
    alertTextField.placeholder = @"email";
    alertTextField.borderStyle = UITextBorderStyleRoundedRect;
    alertTextField.textAlignment = UITextAlignmentCenter;
    alertTextField.font = [UIFont systemFontOfSize:14];
    alertTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    alertTextField.tag = TAG_TEXT_ALERT;
    [alertTextView addSubview:alertTextField];
   // [self.view addSubview:alertTextView];
     
    // Do any additional setup after loading the view from its nib.
}

-(void)parseResponse:(NSNotification *)notification
{
    id response = notification.object;
    NSLog(@"Forgotpasword Response=%@",response);
  if([[response valueForKey:@"CredentialsSent"] boolValue])
  {
      UIAlertView *mailSentAlert = [[UIAlertView alloc]initWithTitle:@"City On Sale" message:@"Your password is sent to your email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
      [mailSentAlert show];
  }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated]; 
    userNameField.text=@"";
    passwordField.text=@"";
    
   
}   

#pragma mark - IBActions

-(IBAction)returnKeyPressed:(id)sender
{
    [super returnKeyPressed:sender];
}
-(IBAction)checkBoxClicked:(id)sender
{
   [checkBoxBtn setSelected: checkBoxBtn.isSelected ? NO : YES ];
    
    if(checkBoxBtn.isSelected)
    {
          [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"RememberMe"];
        
        
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"RememberMe"];
        
    }
    [[NSUserDefaults standardUserDefaults] synchronize];  
   
    
}
-(IBAction)registerUserPressed:(id)sender
{
    RegisterViewController *cntrl = [[RegisterViewController alloc]initWithNibName:nil bundle:nil];
    [self presentModalViewController:cntrl animated:YES];
}
-(IBAction)loginBtnPressed:(id)sender
{
    
    username = userNameField.text;
    password = passwordField.text;
    
    if((username.length != 0) && (password.length != 0))  
    {
        // data in both fields, check for valid email

        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
        if (![emailTest evaluateWithObject:userNameField.text])
        {
            // not a valid email, dont call webservice
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Not a valid email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];  
            
        }
        else {
            //call webservice
            [activityindicator startAnimating];
           
            dispatch_async(kBgQueue, ^{
                
                
               requestUrl = [NSString stringWithFormat:@"http://www.cityonsale.in/User/Logon?username=%@&password=%@",username,password];   
                NSLog(@"%@",requestUrl);
                
                NSError *error = nil;
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:  requestUrl] options:0 error:&error];
                networkError = error;
                [ self performSelectorOnMainThread:@selector(LoginResponse:) 
                                        withObject:data waitUntilDone:YES]; 
                
            });    

            
            
        }

    }
    else {
        // one of the fileds in empty
        UIAlertView *loginAlert = [[UIAlertView alloc]initWithTitle:@"Login Failure" message:@"Missing email or password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [loginAlert show];
    }
} 
 

-(void)LoginResponse:(NSData *)responseData
{
       
    [activityindicator stopAnimating];
    if(networkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"Locations=%@",jsonString);
        NSDictionary *jsonDict = [parser objectWithString:jsonString error:&error]; 
        if(error == nil)
        {
            NSString *successStatus = [jsonDict valueForKey:@"Success"];
            NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
            if([successStatus boolValue])
            {
                //success is true
                   if([errorMessage isEqualToString:@"No Error"])                {
                    NSString *LoginResponse=[jsonDict valueForKey:@"Login"];
                    if([LoginResponse boolValue])
                    {
                          
                        NSString *isStoreOwner=[jsonDict valueForKey:@"IsStoreOwner"];
                        auothtoken = [jsonDict valueForKey:@"AuthToken"];
                        NSLog(@"auothtoken=%@",auothtoken);
                        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"isLogin"];
                        [[NSUserDefaults standardUserDefaults] setObject:auothtoken forKey:@"Auothtoken"];
                        [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"username"];
                         [[NSUserDefaults standardUserDefaults] setValue:isStoreOwner  forKey:@"isStoreOwner"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [self dismissModalViewControllerAnimated:YES];
                       
                        self.tabBarController.selectedIndex = 0;
                         [_appDeleg hideLogin];
                                               
                    }
                    else
                    {
                        UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:[jsonDict valueForKey:@"errorMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                        [errorMessageAlert show];
   
                    }
                    
                    
                }
                else {
                    UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [errorMessageAlert show];
                }
            }
            else
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
        }
        else {
            NSLog(@"get Locations Parsing Error");
        }
        
        
    }

    
    
}


-(IBAction)forgotPassword:(id)sender
{
  
    UITextField *alertTextField = (UITextField *) [alertTextView viewWithTag:TAG_TEXT_ALERT];
    alertTextField.text = nil;
    [alertTextView show];
        
    
        
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
   
   UITextField *alertTextField = (UITextField *) [alertView viewWithTag:TAG_TEXT_ALERT];

    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
    if (![emailTest evaluateWithObject:alertTextField.text])
    {
        // not a valid email, dont call webservice
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Not a valid email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];  
        
    }
    else {
        //call the service here
        
        requestUrl = [NSString stringWithFormat:@"%@?email=%@",forgotPassworddUrl,alertTextField.text];
        
       // NSLog(@"Request Url=%@",requestUrl);
        [self callServiceWiithUrlString];
        serviceEndNotifName = @"ForgotPasswordNotification";

        
    }
    
   
}

-(IBAction)editingChanged:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    
    if(textField.tag == 1)
    {
        if (textField.text.length > 254) 
        {
            textField.text = [textField.text substringToIndex:254];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Maximum 254 characters allowed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            
        }
        
    }

    if(textField.tag == 2)
    {
        if ([textField.text length] > 12) 
        {
            textField.text = [textField.text substringToIndex:12];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Maximum 12 characters allowed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
            
        }
        
    }
}






#pragma mark - orientations
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
