//
//  StoreOwnerViewController.h
//  CityOnSale
//
//  Created by Jagsonics on 26/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"

@interface StoreOwnerViewController : SuperViewController
{
    IBOutlet UIView *bgView;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UITextField *codeField;

}

@end
