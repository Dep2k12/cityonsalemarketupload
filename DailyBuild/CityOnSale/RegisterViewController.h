//
//  RegisterViewController.h
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"
#import  <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface RegisterViewController : SuperViewController<UIPickerViewDelegate,UITextFieldDelegate>
{
    IBOutlet UIView *registerBgView;
    IBOutlet UITextField *firstNameField;
    IBOutlet UITextField *lastNameField;
    IBOutlet UITextField *emailField;
    IBOutlet UITextField *passwordField;
    IBOutlet UITextField *phoneNoField;
    IBOutlet UITextField *location;
    IBOutlet UIButton *locationBtn;
    IBOutlet UIButton *registerBTN;
  //  IBOutlet UIActivityIndicatorView *activityIndicator;
    BOOL isShiftedUp;
    
    UIView * keyboardHeadView;
    NSArray *locationsArray;
    NSArray *locationsIDArray;
    IBOutlet UIPickerView *locationPick;
    NSString * locationID;
    NSString *locationString;
    IBOutlet UIView *Cview;
    IBOutlet UIButton *CviewButton;
    IBOutlet UILabel *locationLabel;
     NSString *auothtoken;
    AppDelegate *_appdel;
    int existFlage;
    NSInteger rowInt;
    IBOutlet UIScrollView *regScroll;
    
  
}

-(IBAction)returnKeyPressed:(id)sender;
-(IBAction)resetKeyPressed:(id)sender;
-(IBAction)backButtonClick:(id)sender;
-(IBAction)registerButtonPressed:(id)sender;
-(void)fetchedregisterresponse:(NSData *)responseData;
-(IBAction)DisplayRound:(id)sender;
-(IBAction)done:(id)sender;
-(IBAction)ValidateUser;
-(void)FetchUserExitingresponse:(NSData*)responseData;


@end
