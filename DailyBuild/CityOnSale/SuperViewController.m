//
//  SuperViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SuperViewController.h"
#import "SBJson.h"

@interface SuperViewController ()

@end

@implementation SuperViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)returnKeyPressed:(id)sender
{

    UITextField *textView = (UITextField *)sender;
    NSString *trimmedText= [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(trimmedText.length == 0)
    {
        textView.text = @"";
        
    }
    else {
        [sender resignFirstResponder];

        UIView *nextResponder = [textView.superview viewWithTag:textView.tag+1];
        if(nextResponder)
        {
            [nextResponder becomeFirstResponder];
        }

    }    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
	// Do any additional setup after loading the view.
    //[self.navigationController set
    networkError = nil;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
-(IBAction)backButtonClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) callServiceWiithUrlString
{
    [activityIndicator startAnimating];
    dispatch_async(kBgQueue, ^{
        //(string username, string marketID, string categoryID, string authToken)
        
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:requestUrl] options:0 error:&error];
        networkError = error;
      //  NSLog(@"Network Error=%@",networkError);
        [ self performSelectorOnMainThread:@selector(createJsonFromData:) 
                                withObject:data waitUntilDone:YES]; 
        
    });     
}

-(void) createJsonFromData:(NSData *)responseData
{
    [activityIndicator stopAnimating];
    
    if(networkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
       // NSLog(@"getStores=%@",jsonString);
        NSString *jsonDict = [parser objectWithString:jsonString error:&error]; 
        
        NSString *successStatus = [jsonDict valueForKey:@"Success"];
        NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
        if([successStatus boolValue])
        {
            //success is true
            if([errorMessage isEqualToString:@"No Error"])
            {
                //  categoriesArray = [jsonDict valueForKey:@"Categories"];
                // [myTableView reloadData];
                [[NSNotificationCenter defaultCenter] postNotificationName:serviceEndNotifName object:jsonDict];
                
                
            }
            else {
                UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [errorMessageAlert show];
            }
        }
        else
        {
            UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [successAlert show];
        }
    }
    
    
    
}



@end
