//
//  StoreDetailViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StoreDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "SBJson.h"
#import "BestDealsViewController.h"
#import "StoreListViewController.h"
#import "WhatsHotViewController.h"
#import "NSData+NSDataAdditions.h"

#define TAG_SCROLL_IMAGE 1

@interface StoreDetailViewController ()


@end

@implementation StoreDetailViewController
@synthesize storeId,storeName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parseResponse:) name:@"GetCouponNotification" object:nil];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GetCouponNotification" object:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    navTitle.text = storeName;
    // Do any additional setup after loading the view from its nib.
    
       bgCenterView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"store-detailbg"]];
    
    CGAffineTransform rotateTable = CGAffineTransformMakeRotation(-M_PI_2);
	myTableView.transform = rotateTable;
	myTableView.frame = CGRectMake(0, 278, myTableView.frame.size.width, myTableView.frame.size.height);
    
    NSString *authToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    //GetStoreDetail(string username, string storeID, string authToken)
    dispatch_async(kBgQueue, ^{
        //(string username, string marketID, string categoryID, string authToken)
        requestUrl = [NSString stringWithFormat:@"%@?username=%@&storeID=%@&authToken=%@",getStoreDetailUrl,userName,storeId,authToken];
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:  requestUrl] options:0 error:&error];
        networkError = error;
        [ self performSelectorOnMainThread:@selector(fetchedStoresDetailData:) 
                                withObject:data waitUntilDone:YES]; 
        
    });    
    NSArray *cntrlsArray = [self.navigationController viewControllers];
    NSString *updataURL = nil;
    
   // NSLog(@"cntrlsArray=%@",cntrlsArray);
    
    id previoiusVc = [cntrlsArray objectAtIndex:[cntrlsArray count]-2];
   // NSLog(@"PrevousVC=%@",previoiusVc);
    
  /*  if([previoiusVc isKindOfClass:[StoreListViewController class]])
    {
        updataURL = getregularDealUpdatesUrl;
        
    }
    else if ([previoiusVc isKindOfClass:[WhatsHotViewController class]])
    {
        updataURL = getHotDealUpdatesUrl;
    }
    else {
        // from best deals
        updataURL = getBestDealUpdatesUrl;
    }*/

    updataURL = getregularDealUpdatesUrl;
    dispatch_async(kBgQueue, ^{
        //(string username, string marketID, string categoryID, string authToken)
      requestUrl = [NSString stringWithFormat:@"%@?username=%@&storeID=%@&authToken=%@",updataURL,userName,storeId,authToken];
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:  requestUrl] options:0 error:&error];
        networkError = error;
        [ self performSelectorOnMainThread:@selector(fetchedUpdateData:) 
                                withObject:data waitUntilDone:YES]; 
        
    });    
    
    dispatch_async(kBgQueue, ^{
        //(string username, string marketID, string categoryID, string authToken)
        
        requestUrl = [NSString stringWithFormat:@"%@?username=%@&storeID=%@&authToken=%@",getSotrePhotosUrl,userName,storeId,authToken];
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:  requestUrl] options:0 error:&error];
        networkError = error;
        [ self performSelectorOnMainThread:@selector(fetchedPhotosData:) 
                                withObject:data waitUntilDone:YES]; 
        
    });    

    
}
-(void)fetchedPhotosData:(NSData *)responseData
{
    [activityIndicator stopAnimating];
    
    if(networkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
      //  NSLog(@"getPhotos=%@",jsonString);
        NSDictionary *jsonDict = [parser objectWithString:jsonString error:&error]; 
        if(error == nil)
        {
            NSString *successStatus = [jsonDict valueForKey:@"Success"];
            NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
            if([successStatus boolValue])
            {
                //success is true
                   if([errorMessage isEqualToString:@"No Error"])                {
                   imageUrlArray =  [jsonDict valueForKey:@"StorePhotos"];
                    if( (imageUrlArray == (id)[NSNull null]) || ([imageUrlArray count] == 0))
                    {
                             noPhotoLabel.text = @"No photos available";
                    }
                    else {
                          [myTableView reloadData];
                    }
                        
                  
                }
                else {
                    UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [errorMessageAlert show];
                }
            }
            else
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
        }
        else {
          //  NSLog(@"get Locations Parsing Error");
        }
        
        
    }
    
}
-(void)fetchedUpdateData:(NSData *)responseData
{
   // [activityIndicator stopAnimating];
    
    if(networkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
       // NSLog(@"getUpdatesl=%@",jsonString);
        NSDictionary *jsonDict = [parser objectWithString:jsonString error:&error]; 
        if(error == nil)
        {
            NSString *successStatus = [jsonDict valueForKey:@"Success"];
            NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
            if([successStatus boolValue])
            {
                //success is true
                   if([errorMessage isEqualToString:@"No Error"])
                {
                   /* NSArray *cntrlsArray = [self.navigationController viewControllers];
                    id previoiusVc = [cntrlsArray objectAtIndex:[cntrlsArray count]-2];
                    NSMutableString *bulletedUpdate = [[NSMutableString alloc]init];
                    
                    NSString *updateArrayKey = nil;
                    NSString *updateTextKey = nil;
                    
                    if([previoiusVc isKindOfClass:[StoreListViewController class]])
                    {
                        updateArrayKey = @"RegularUpdates";
                        updateTextKey = @"RegularUpdateText";
                        
                    }
                    else if ([previoiusVc isKindOfClass:[WhatsHotViewController class]])
                    {
                       updateArrayKey = @"HotDealUpdates";
                        updateTextKey = @"HotDealText";
                    }
                    else {
                        // from best deals
                        updateArrayKey = @"BestDealUpdates";
                        updateTextKey = @"BestDealText";
                    }*/
                    NSMutableString *bulletedUpdate = [[NSMutableString alloc]init];
                    NSString *updateArrayKey = nil;
                    NSString *updateTextKey = nil;

                    updateArrayKey = @"RegularUpdates";
                    updateTextKey = @"RegularUpdateText";
                    
                    NSArray *updateArray =  [jsonDict valueForKey:updateArrayKey];
                    for(int i=1;i<=[updateArray count];i++)
                    {
                        [bulletedUpdate appendString:[NSString stringWithFormat:@">>  %@ \n",[[updateArray objectAtIndex:i-1]valueForKey:updateTextKey]]];
                    }
                storeUpdatesTextView.text = bulletedUpdate;   
                                }
                else {
                   // UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    //[errorMessageAlert show];
                    storeUpdatesTextView.text = @"No Data";  
                }
            }
            else
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
        }
        else {
            //NSLog(@"get Locations Parsing Error");
        }
        
        
    }
    
}


-(void)fetchedStoresDetailData:(NSData *)responseData
{
   // [activityIndicator stopAnimating];
    
    if(networkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Network Problem" message:@"Try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
       // NSLog(@"getStoreDetail=%@",jsonString);
        NSDictionary *jsonDict = [parser objectWithString:jsonString error:&error]; 
        if(error == nil)
        {
            NSString *successStatus = [jsonDict valueForKey:@"Success"];
            NSString *errorMessage = [jsonDict valueForKey:@"errorMessage"];
            if([successStatus boolValue])
            {
                //success is true
                   if([errorMessage isEqualToString:@"No Error"])
                {
                    //getStoreDetail={"Success":true,"StoreDetail":{"ID":1,"StoreName":"Cafe Cofee Day","StoreAddress":"Main Road,Saket,New Delhi","StoreDescription":null,"StorePhoneNumber":null,"TypeName":"Cafe","StorePrice":"100-200","StoreOffering":"10-20"},"errorMessage":"No Error","tag":"GetStoreDetail"}
                    
                    NSDictionary *storeDetail = [jsonDict valueForKey:@"StoreDetail"];
                 //   NSString *storeName = [storeDetail valueForKey:@"StoreName"];
                    NSString *storeAddress = [storeDetail valueForKey:@"StoreAddress"];
                    NSString *storeDescription = [storeDetail valueForKey:@"StoreDescription"];
                    NSString *storePhoneNumber = [storeDetail valueForKey:@"StorePhoneNumber"];
                    
                    NSString *storeTypeName = [storeDetail valueForKey:@"TypeName"];
                    NSString *storePrice = [storeDetail valueForKey:@"StorePrice"];
                    NSString *storeLogoData = [storeDetail valueForKey:@"StoreLogo"];
                    
                   // NSString *storeOffering =  [storeDetail valueForKey:@"StoreOffering"];
                    
                    
                    storeTypeLabel.text = ((storeTypeName == (id)[NSNull null]) ? @"No Data":storeTypeName);
                    storePriceLabel.text = storePrice==(id)[NSNull null]? @"No Data":storePrice;

                    storePhoneNumberLabel.text = storePhoneNumber== (id)[NSNull null]? @"No Data":storePhoneNumber;

                    //storeAddressLabel.text = storeAddress==(id)[NSNull null]? @"No Data":storeAddress;
                    
                    if(storeAddress ==(id)[NSNull null])
                    {
                        if(storePhoneNumber == (id)[NSNull null])
                        {
                            storeAddressLabel.text = @"No Data";
                        }
                        else {
                            storeAddressLabel.text = [NSString stringWithFormat:@"Ph No: %@",storePhoneNumber];
                            
                        }
                    }
                    else {
                        storeAddressLabel.text = [NSString stringWithFormat:@"%@ Ph No: %@",storeAddress,storePhoneNumber];
                    }

                    storeDescriptionTextView.text = storeDescription==(id)[NSNull null]? @"No Data":storeDescription;
                    
                    if(!(storeLogoData ==(id)[NSNull null]))
                    {
                        
                        
                        NSData *imageData = [NSData decodeBase64WithString:storeLogoData];
                        [storeLogoView setImage:[UIImage imageWithData:imageData]];
                    }
                
                        

                }
                else {
                    UIAlertView *errorMessageAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [errorMessageAlert show];
                }
            }
            else
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:successStatus delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
        }
        else {
          //  NSLog(@"get Locations Parsing Error");
        }
        
        
    }
    
}

#pragma mark - tableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    return [imageUrlArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
        
        UIImageView *scrollImageView;
        
       
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) 
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
            
            scrollImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,5, 55,55)];
            
       
            
            scrollImageView.userInteractionEnabled = YES;
            scrollImageView.tag =TAG_SCROLL_IMAGE;
            
            //  productImageView.layer.cornerRadius = 10;
            scrollImageView.layer.borderWidth = 1;
            scrollImageView.layer.borderColor = [UIColor blackColor].CGColor;
            
            
            scrollImageView.contentMode = UIViewContentModeScaleToFill;
            
            CGAffineTransform rotateImage = CGAffineTransformMakeRotation(M_PI_2);
            scrollImageView.transform = rotateImage;
            
            
            [cell.contentView addSubview:scrollImageView];
            
        }
        else {
            scrollImageView = (UIImageView*)[cell.contentView viewWithTag:TAG_SCROLL_IMAGE];
        }
    //convert  data to image
    
    NSString *imageUrl = [[imageUrlArray objectAtIndex:indexPath.row]valueForKey:@"StorePhotoData"];
    if(imageUrl ==(id)[NSNull null] )
    {
        [scrollImageView setImage:[UIImage imageNamed:@"No-Image-Available.jpg"]];
    }
    else {
        NSData *imageData = [NSData decodeBase64WithString:imageUrl];
        
        [scrollImageView setImage:[UIImage imageWithData:imageData]];  
    }
  //  [scrollImageView  setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"photofram"]];
    return cell;
    
}
-(IBAction)phoneCall
{
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",storePhoneNumberLabel.text]]];
      //  storePhoneNumberLabel.text = @"9999741976";
        NSString *cleanedString = [[storePhoneNumberLabel.text componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", escapedPhoneNumber]];
        [[UIApplication sharedApplication] openURL:telURL];
    } else {
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"City on Sale" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
      
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction)getCoupon:(id)sender
{
    //string username, string storeID, string authToken
    //super class implementattion
    authToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"Auothtoken"];
    userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    requestUrl = [NSString stringWithFormat:@"%@?username=%@&storeID=%@&authtoken=%@",getCouponCodeUrl,userName,storeId,authToken];
    
    [self callServiceWiithUrlString];
    serviceEndNotifName = @"GetCouponNotification";
    

}


-(void)parseResponse:(NSNotification *)notification
{
    id response = notification.object;
   // NSLog(@"response=%@",response);
    couponCode = [response valueForKey:@"CouponCode"];
   ;
    
    if([[response valueForKey:@"CouponGenerated"] boolValue])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Hurray !! Your coupon code successfully generated" message:couponCode delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Failed to generate coupon code" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
   // codeBtn.titleLabel.text = [NSString stringWithFormat:@"Coupon Code: %@",couponCode];
    [codeBtn setHidden:YES];
    [codeLabel setHidden:NO];
   [ codeLabel setText:[NSString stringWithFormat:@"Coupon Code: %@",couponCode]];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
