//
//  NSString+NSStringAdditions.h
//  SyncMa
//
//  Created by  Abhi on 3/22/12.
//  Copyright (c) 2012   Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/NSString.h>

@interface NSString (NSStringAdditions)

+ (NSString *) base64StringFromData:(NSData *)data length:(int)length;

+ (NSString *)encodeBase64WithString:(NSString *)strData;

+ (NSString *)encodeBase64WithData:(NSData *)objData;

@end

