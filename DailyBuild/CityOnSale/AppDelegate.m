//
//  AppDelegate.m
//  CityOnSale
//
//  Created by Jagsonics on 13/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import  "StoreOwnerViewController.h"
#import "Parse/Parse.h"

@implementation AppDelegate
@synthesize window = _window;
@synthesize tabBarController = _tabBarController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    /* Parse ----------------------*/
    
//    
     [Parse setApplicationId:@"hGe1pRmDkQiUKUTCiZwmkkkrKtTkWxajZ2wAiREb"clientKey:@"Q5cw9RMUc7o20bE04XF42d7kHO9ThmP7EJm8qYnA"];
    
    
    
//    [PFUser enableAutomaticUser];
//    PFACL *defaultACL = [PFACL ACL];
//    // Optionally enable public read access by default.
//    // [defaultACL setPublicReadAccess:YES];
//    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
   // [PFPush subscribeToChannelInBackground:@""];

    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|
     UIRemoteNotificationTypeAlert|
     UIRemoteNotificationTypeSound];
        
    /* end Parse --------------------*/
    
    
    
    
    
    
    
    
    // Override point for customization after application launch.
    UIViewController  *homeViewController, *searchViewController, 
    *bestDealsViewController,*whatsHotViewController,* profileViewController,*storeOwnerViewController;
    
    _loginView =[[LoginViewController alloc] initWithNibName:@"LoginViewController" 
                                                      bundle:nil];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
    {
        
        homeViewController = [[HomeViewController alloc]initWithNibName:nil bundle:nil]; 
        searchViewController = [[SearchViewController alloc] initWithNibName:nil bundle:nil];
        bestDealsViewController = [[BestDealsViewController alloc]initWithNibName:nil bundle:nil];
        whatsHotViewController = [[WhatsHotViewController alloc]initWithNibName:nil bundle:nil];
        profileViewController =[[ProfileViewController alloc]initWithNibName:nil bundle:nil];
        // profileViewController =[[ProfileViewController alloc]initWithNibName:nil bundle:nil];
        
        storeOwnerViewController = [[StoreOwnerViewController alloc]initWithNibName:nil bundle:nil];
        
        
        
        
        navigationControllerHome = [[UINavigationController alloc]initWithRootViewController:homeViewController];
        navigationControllerSearch = [[UINavigationController alloc]initWithRootViewController:searchViewController];
        navigationControllerBestDeals = [[UINavigationController alloc]initWithRootViewController:bestDealsViewController];
        navigationControllerWhatsHot= [[UINavigationController alloc]initWithRootViewController:whatsHotViewController];
        navigationControllerProfile = [[UINavigationController alloc]initWithRootViewController:profileViewController];
        navigationControllerStoreOwner = [[UINavigationController alloc]initWithRootViewController:storeOwnerViewController];     
        
    } else {
        
    }
    
    self.tabBarController = [[UITabBarController alloc] init];
    
    
    
    NSString *isLogin = [[NSUserDefaults standardUserDefaults] valueForKey:@"isLogin"];
    if(isLogin == nil)
    {
        [self showLogin];
        
    }
    else {
        NSString *rem=  [[NSUserDefaults standardUserDefaults] objectForKey:@"RememberMe"];
      
        if([rem boolValue])
        {
            [self hideLogin];  // donot show login, 
        }
        else {
            
            
            [self showLogin];
            
        }
        
    }
    
    
    //self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    //Parse implementation
    
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor redColor]];
    return YES;
    

    
    
}
-(void)showLogin{
    self.window.rootViewController = _loginView;
}
-(void)hideLogin{
    
    NSString *isStoreOwner =  [[NSUserDefaults standardUserDefaults]valueForKey:@"isStoreOwner"];
    if([isStoreOwner boolValue])
    {
        // show only two tabs
        self.tabBarController.viewControllers = [NSArray arrayWithObjects:navigationControllerStoreOwner,navigationControllerProfile, nil];
        self.window.rootViewController = self.tabBarController;
        self.tabBarController.selectedIndex=0;
    }
    else
    {
        self.tabBarController.viewControllers = [NSArray arrayWithObjects:navigationControllerHome,navigationControllerSearch,navigationControllerBestDeals,navigationControllerWhatsHot,navigationControllerProfile, nil];
        
        self.window.rootViewController = self.tabBarController;
        self.tabBarController.selectedIndex=0;
        [navigationControllerHome popToRootViewControllerAnimated:NO];
        
    }
    
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
    
  }

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
    
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}




// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo 
{
    [PFPush handlePush:userInfo];
    NSLog(@"Notification %@",userInfo); 
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)subscribeFinished:(NSNumber *)result error:(NSError *)error {
    if ([result boolValue]) {
        NSLog(@"ParseStarterProject successfully subscribed to push notifications on the broadcast channel.");
    } else {
        NSLog(@"ParseStarterProject failed to subscribe to push notifications on the broadcast channel.");
    }
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    [PFPush storeDeviceToken:deviceToken]; // Send parse the device token
    [PFPush subscribeToChannelInBackground:@""];
    
}
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *) error {
    if ([error code] == 3010) {
       NSLog(@"Push notifications don't work in the simulator!");
       } else {
           NSLog(@"didFailToRegisterForRemoteNotificationsWithError: %@", error);
            }
}

/*
 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
 {
 }
 */

@end
