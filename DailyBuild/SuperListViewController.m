//
//  SuperListViewController.m
//  CityOnSale
//
//  Created by Jagsonics on 23/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SuperListViewController.h"
#import "SBJson.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSData+NSDataAdditions.h"
@interface SuperListViewController ()

@end

@implementation SuperListViewController

@synthesize categoryId,marketId,navTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) parseResponse:(NSNotification *)notification
{
    NSDictionary *jsonDict = (NSDictionary *)notification.object;
    storesArray = [jsonDict valueForKey:@"Stores"];
    if((storesArray == (id)[NSNull null]) || ([storesArray count] == 0))
    {
        UIAlertView *noDataAlert = [[UIAlertView alloc]initWithTitle:@"City on Sale" message:@"Store not found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noDataAlert show];
    }
    else {
        [storesTable reloadData];
    }

}

                                                        
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    serviceEndNotifName = @"serviceEndedNotif";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(parseResponse:) name:serviceEndNotifName object:nil];
    // Do any additional setup after loading the view from its nib.
    //call the service here
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"MasterBg"]];
    navTitleLabel.text = navTitle;
    [super callServiceWiithUrlString];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
        
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - tableMethods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [storesArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"CellIdentifier";
    
	
	StoreListCell *storeListCell = (StoreListCell *)[tableView 
                                                     dequeueReusableCellWithIdentifier: cellIdentifier];
    if (storeListCell == nil)  
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StoreListCell" owner:self options:nil];
        
        for (id oneObject in nib)
            if ([oneObject isKindOfClass:[StoreListCell class]])
                storeListCell = (StoreListCell *)oneObject;
        storeListCell.selectionStyle = UITableViewCellSelectionStyleNone;
        storeListCell.storeBgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"store-rowbg"]];
    }    
    
    NSString *storeName = [[storesArray objectAtIndex:indexPath.row]valueForKey:@"StoreName"];
    NSString *storeType = [[storesArray objectAtIndex:indexPath.row] valueForKey:@"TypeName"];
    NSString *storeDiscount = [[storesArray objectAtIndex:indexPath.row]valueForKey:@"StoreOffering"];
    NSString *storePrice=[[storesArray objectAtIndex:indexPath.row]valueForKey:@"StorePrice"];
    
    
    NSString *storeLogoData = [[storesArray objectAtIndex:indexPath.row]valueForKey:@"StoreLogo"];
    
    storeListCell.storeName.text =   ((storeName == (id)[NSNull null]) ? @"No Data" : storeName);
    storeListCell.storeDiscount.text = ((storeDiscount == (id)[NSNull null]) ? @"No Data" :  [NSString stringWithFormat:@"%@ Offered",storeDiscount ]);
    storeListCell.storePriceRange.text = ((storePrice == (id)[NSNull null]) ? @"No Data" : [NSString stringWithFormat:@"Price Range: %@",storePrice ]); 
    storeListCell.storeType.text = ((storeType == (id)[NSNull null]) ? @"No Data" : storeType);
    if(storeLogoData != (id)[NSNull null])
    {
        
        
        NSData *imageData = [NSData decodeBase64WithString:storeLogoData];
        [storeListCell.storeLogo setImage:[UIImage imageWithData:imageData]];
    }
    else {
        [storeListCell.storeLogo setImage:[UIImage imageNamed:@"No-Image-Available.jpg"]];

    }
    
    return storeListCell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    NSString *storeId= [[storesArray objectAtIndex:indexPath.row]valueForKey:@"ID"];
    NSString *storeName= [[storesArray objectAtIndex:indexPath.row]valueForKey:@"StoreName"];
    
    StoreDetailViewController *cntrl = [[StoreDetailViewController alloc]initWithNibName:nil bundle:nil];
    
    cntrl.storeId = storeId;
    cntrl.storeName = storeName;
    
    [self.navigationController pushViewController:cntrl
                                         animated:YES];
}

-(IBAction)backButtonClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
